$(function(){
////////////////////////////////////////////////////////////
////// GLOBAL
/////////////////////////////////////////////////////////
var  board     = {}
	,token = {}
	,habit = {}
	;
var  user_token_class= "user_token"
	,pc_token_class = "pc_token";

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// BOARD FUNCTIONS
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
board.init = function(i) {
	b = this;
	this.id ="#board_"+i;
	this.sections = new Array();
	this.mousedown = false;
	this.days = new Array();  //num max column of grid
	this.rows = 4;
	this.cols = 0;
	this.matrix = new Array();
	this.tokens = new Array();
	this.habit = null;
}
board.set = function(days){
	for( i in days){
		this.days.push(days[i]);		
	}
	this.cols = this.days.length;
	for(var i = 0; i < this.rows; i++){
		var d = new Array();
		for(var j = 0; j < this.cols; j++){
			d.push(0);
		}
		this.matrix.push(d);	
	}

	this.buildGrid(); 
	this.interaction();
}
board.print = function(){
	for(var i = 0; i < this.rows; i++){
		for(var j = 0; j < this.cols; j++){
			console.log("["+i+"]["+j+"]:"+this.matrix[i][j]);
		}
	}
}
board.update = function(){
	this.print();
	this.updateCell();	
}
board.updateCell = function(c,r){
	var _r = this.firstVerticalSpot(c,r);
	this.matrix[_r][c] +=1;	
	$('.cell[row='+_r+'][col='+c+']').html(this.matrix[_r][c]);
	$('.cell[row='+_r+'][col='+c+']').addClass(this.tokens[this.tokens.length - 1].active_class);
}
board.buildGrid = function(){
	var html =""
	for( var i = this.rows - 1 ; i >= 0 ; i--){
		html +='<div class="row">';
		for(var j = 0; j < this.cols; j++){
			html +='<div class="span cell span2" row='+i+' col='+j+'>'+this.matrix[i][j]+'</div>';
		}
		html +='</div>'; // close row
	}
	$(".container-fluid").append(html);
	///button #remove when become really interactive
	html ='<div class="row">';
	for(var j = 0; j < this.cols; j++){
		html +='<a class="btn btn-link span2" col='+j+' val="1">Did on '+this.days[j]+'</a>';
	}
	html +='</div>'; // close row
	html +='<div class="row">';
	for(var j = 0; j < this.cols; j++){
		html +='<a class="btn btn-link span2" col='+j+' val="-1">Not on '+this.days[j]+'</a>';
	}
	html +='</div>'; // close row
	$(".container-fluid").append(html);
}
board.beginHabit = function(){
	if(!this.startDate){
		var myDate = new Date();
		this.startDate = myDate.getDate();
	}

}
board.interaction = function(){
	var that = this;
	$("a.btn").click(function(){
		var c = parseInt($(this).attr('col'));
		var v = parseInt($(this).attr('val'));

		//if first token start habit from that day
		if(that.tokens.length == 0){
			that.startHabit(c,v);
			that.updateCell(c);
		}
		if(that.allowedInput(c)){
			that.addToken(c,v);
			that.updateCell(c);
		}
		//addToken if allowed
		
	})
}
board.startHabit = function(c,v){
	//input order starting from this point/day
	console.log("start habit =======");
	habit.init(c);
	this.habit = habit.get();
	this.addToken(c,v);
}
board.addToken = function(c,v){
	this.tokens.push( token.get());
	token.init(this.tokens.length, v);
}
board.allowedInput = function(c){
	//if allowed to insert element on this column checking habit order
	var _r = this.firstVerticalSpot(c);
	var allowed = false;
	if( _r == habit.cellOrder[this.tokens.length].row &&  c == habit.cellOrder[this.tokens.length].col )
		allowed = true;
	else
		allowed = false;
	console.log("allowed"+allowed)
	return allowed;
}
board.firstVerticalSpot = function(c,r){
	if(!r){
		for( j in this.matrix ){
			if(this.matrix[j][c] == 0){
				r = j;
				break;
			}
		}
	}
	return r;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// TOKEN
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
token.init = function( i, v ) {
	b = this;
	this.id ="#token_"+i;
	this.insertData = "";
	this.value = "";
	this.descr = "";
	this.active_class = "";
	this.set(v);
}
token.set = function( v ){
	var myDate = new Date();
	this.insertData = myDate.getDate();
	this.value = v;
	if(this.value == 1){
		this.active_class = user_token_class;
	}
	else if (this.value == -1){
		this.active_class = pc_token_class;
	}
}
token.get = function(){
	return this;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// TOKEN
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
habit.init = function( c ) {
	b = this;
	this.id ="#habit";
	this.insertData = "";
	this.value = "";
	this.descr = "";
	this.cellOrder = new Array();
	this.dayOrder = new Array();
	this.set(c);
	this.order(c);
}
habit.set = function( c){
	var myDate = new Date();
	this.insertData = myDate.getDate();
}
habit.order = function(c){
	var index = 0;
	var rows = board.rows;
	var cols = board.cols;
	for(var i = 0; i < rows; i++){
		for(var j = 0; j < cols; j++){
			index = j+c;
			if(index >= cols){	
				index-= cols;
			}
			this.cellOrder.push({row:i,col:index});
		}
	}
	for(var i in this.cellOrder){
		console.log("["+this.cellOrder[i].row+"] ["+this.cellOrder[i].col+"]")
	}
}
habit.get = function (){
	return this;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// COMMON FUNCTIONS
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// START 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
board.init(0);
board.set(["Mon","Tue","Wen","Thr","Fri"]);

});

